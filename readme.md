# Bootcamp API

> Backend API for a bootcamp directory website.

## Usage

Update the values/settings for every file in folder "src/config/env" to your own.

- File "development.env" contains settings for development mode.
- File "testing.env" contains settings for testing mode.
- File "production.env" contains settings for production mode.
- File "default.env" contains default settings, which are used when the config file of current mode is missing.

## Install Dependencies

```
npm i
```

## Run app

```
# Run in developement mode
npm run dev

# Run in testing mode
npm run test

# Run in production mode
npm start
```

# Bootcamp API specifications
