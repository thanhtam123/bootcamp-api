const fs = require('fs');
const mongoose = require('mongoose');
const colors = require('colors');
const dotenv = require('dotenv');
const path = require('path');

// load env vars
dotenv.config({ path: './src/config/env/development.env' });

// load models
const { Bootcamp, Course, User, Review } = require('./src/models');
const { O_DIRECT } = require('constants');

console.log(process.env.MONGO_URL);

// connect to db
const conn = mongoose.connect(process.env.MONGO_URL, {
	useNewUrlParser: true,
	useCreateIndex: true,
	useFindAndModify: false,
	useUnifiedTopology: true,
});
console.log('Successfully connect to MongoDB'.blue);

// read JSON file
const bootcamps = JSON.parse(
	fs.readFileSync(path.join(__dirname, './_data/bootcamps.json'), 'utf-8')
);
const courses = JSON.parse(
	fs.readFileSync(path.join(__dirname, './_data/courses.json'), 'utf-8')
);

const users = JSON.parse(
	fs.readFileSync(path.join(__dirname, './_data/users.json'), 'utf-8')
);

const reviews = JSON.parse(
	fs.readFileSync(path.join(__dirname, './_data/reviews.json'), 'utf-8')
);

// Import into db
const importData = async () => {
	try {
		await User.create(users);
		await Bootcamp.create(bootcamps);
		await Course.create(courses);
		await Review.create(reviews);

		console.log('Data Imported...'.green.inverse);
		process.exit();
	} catch (err) {
		console.error(err);
	}
};

// delete data
const deleteData = async () => {
	try {
		await Bootcamp.deleteMany();
		await Course.deleteMany();
		await User.deleteMany();
		await Review.deleteMany();

		console.log('Data Destroy...'.red.inverse);
		process.exit();
	} catch (err) {
		console.error(err);
	}
};

if (process.argv[2] === '-i') {
	importData();
} else if (process.argv[2] === '-d') {
	deleteData();
}
