const performQuery = (schema) => {
	schema.statics.performQuery = async function (
		queryParams = {},
		populate = undefined
	) {
		let query;

		// copy queryParams
		const reqQuery = { ...queryParams };

		// Fields to exclude
		const removeFields = ['select', 'sort', 'page', 'limit'];

		// Loop over removeFields and delete them from reqQuery
		removeFields.forEach((param) => delete reqQuery[param]);

		// Create query string
		let queryStr = JSON.stringify(reqQuery);

		// Create operators ($gt, $gte, etc)
		queryStr = queryStr.replace(
			/\b(gt|gte|lt|lte|in)\b/g,
			(match) => `$${match}`
		);

		// Finding resource
		query = this.find(JSON.parse(queryStr));

		// Select Fields
		if (queryParams.select) {
			const fields = queryParams.select.split(',').join(' ');
			query = query.select(fields);
		}

		// Sort
		if (queryParams.sort) {
			const sortBy = queryParams.sort.split(',').join(' ');
			query = query.sort(sortBy);
		} else {
			query = query.sort('-createdAt');
		}

		// Pagination
		const page = parseInt(queryParams.page, 10) || 1;
		const limit = parseInt(queryParams.limit, 10) || 25;
		const startIndex = (page - 1) * limit;
		const endIndex = page * limit;
		const total = await this.countDocuments(JSON.parse(queryStr));

		query = query.skip(startIndex).limit(limit);

		if (populate) {
			query = query.populate(populate);
		}

		// Executing query
		const results = await query;

		// Pagination result
		const pagination = {};

		if (endIndex < total) {
			pagination.next = {
				page: page + 1,
				limit,
			};
		}

		if (startIndex > 0) {
			pagination.prev = {
				page: page - 1,
				limit,
			};
		}

		return { count: results.length, pagination, data: results };
	};
};

module.exports = performQuery;
