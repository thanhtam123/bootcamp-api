const mongoose = require('mongoose');

const UserTokenSchema = new mongoose.Schema(
	{
		user: {
			type: mongoose.Schema.Types.ObjectId,
			required: true,
		},
		type: {
			type: String,
			required: true,
		},
		token: {
			type: String,
			required: true,
		},
		expireAt: {
			type: Date,
			required: true,
		},
	},
	{
		timestamps: true,
	}
);

UserTokenSchema.index({ expireAt: 1 }, { expireAfterSeconds: 0 });

module.exports = mongoose.model('UserToken', UserTokenSchema);
