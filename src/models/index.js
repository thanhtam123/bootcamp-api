module.exports = {
	Bootcamp: require('./bootcamp.model'),
	Course: require('./course.model'),
	User: require('./user.model'),
	Review: require('./review.model'),
};
