const express = require('express');
const path = require('path');
const httpStatus = require('http-status');
const cors = require('cors');
const fileupload = require('express-fileupload');
const cookieParser = require('cookie-parser');
const mongoSanitize = require('express-mongo-sanitize');
const helmet = require('helmet');
const xss = require('xss-clean');
const rateLimit = require('express-rate-limit');
const hpp = require('hpp');
// const morgan = require('morgan');

const ApiError = require('../utils/ApiError');
const {
	errorConverter,
	errorHandler,
} = require('../middlewares/error.middleware');
const morgan = require('../config/morgan');

const app = express();

// parse application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// parse application/json
app.use(express.json());

// enable cors
app.use(cors());
app.options('*', cors());

// cookie parser
app.use(cookieParser());

// dev logging middleware
if (process.env.NODE_ENV !== 'testing') {
	app.use(morgan.successHandler);
	app.use(morgan.errorHandler);
}

// file upload
app.use(fileupload());

// Sanitize data, prevent noSQL injection
app.use(mongoSanitize());

// Set security header
app.use(helmet());

// Prevent xss attacks
app.use(xss());

// Rate limiting
const limiter = rateLimit({
	windowMs: 10 * 160 * 1000, // 10 mins
	max: 100,
});
app.use(limiter);

// Prevent http params pollution
app.use(hpp());

app.use(express.static(path.join(__dirname, '../public')));

// simple route
app.get('/', async (req, res, next) => {
	res.sendFile(path.join(__dirname, '../../public/index.html'));
	// res
	// 	.status(httpStatus.OK)
	// 	.send({ success: true, message: 'Welcome to Bootcamp-API' });
});

// config router
app.use('/', require('../routes')());

// send back a 404 error for any unknown api request
app.use(async (req, res, next) => {
	next(new ApiError(httpStatus.NOT_FOUND, 'Not found route.'));
});

// convert error to ApiError, if needed
app.use(errorConverter);

// handle error
app.use(errorHandler);

module.exports = app;
