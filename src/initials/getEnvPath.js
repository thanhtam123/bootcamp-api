const path = require('path');

const getEnvPath = () => {
	const { NODE_ENV } = process.env;

	let envPath = '../config/env/default.env';
	switch (NODE_ENV) {
		case 'development':
			envPath = '../config/env/development.env';
			break;

		case 'testing':
			envPath = '../config/env/testing.env';
			break;

		case 'production':
			envPath = '../config/env/production.env';
			break;

		default:
			envPath = '../config/env/default.env';
			break;
	}
	envPath = path.join(__dirname, envPath);
	return envPath;
};

module.exports = getEnvPath;
