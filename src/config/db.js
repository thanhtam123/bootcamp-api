const mongoose = require('mongoose');
const logger = require('./logger');

const connectDB = async () => {
	const conn = await mongoose.connect(process.env.MONGO_URL, {
		useNewUrlParser: true,
		useCreateIndex: true,
		useFindAndModify: false,
		useUnifiedTopology: true,
	});
	logger.info('Successfully connect to MongoDB');
	return conn;
};

module.exports = { connectDB };
