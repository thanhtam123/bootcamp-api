const winston = require('winston');
require('winston-mongodb');
const path = require('path');

const enumerateErrorFormat = winston.format((info) => {
	if (info instanceof Error) {
		Object.assign(info, { message: info.stack });
	}
	return info;
});

const configTransports = () => {
	if (process.env.NODE_ENV === 'production') {
		// log in mongodb
		return [
			new winston.transports.MongoDB({
				db: `${process.env.MONGO_URL}`,
				username: 'thanhtam',
				password: 'thanhtam123',
				collection: 'log-info',
				level: 'info',
				capped: true,
				options: {
					useNewUrlParser: true,
					// useCreateIndex: true,
					// useFindAndModify: false,
					useUnifiedTopology: true,
				},
			}),
		];
	} else {
		return [
			// log in console
			new winston.transports.Console({
				stderrLevels: ['error'],
			}),

			// log in file
			new winston.transports.File({
				filename: path.join(__dirname, '../../log/error.log'),
				level: ['error'],
			}),
			new winston.transports.File({
				filename: path.join(__dirname, '../../log/info.log'),
			}),
		];
	}
};

const logger = winston.createLogger({
	level: process.env.NODE_ENV === 'development' ? 'debug' : 'info',

	// format của log được kết hợp thông qua format.combine
	format: winston.format.combine(
		enumerateErrorFormat(),
		// Định dạng time cho log
		winston.format.timestamp({
			format: 'DD-MM-YYYY HH:mm:ss',
		}),

		// cause error when log, unknown to fix
		// process.env.NODE_ENV === 'development'
		// 	? winston.format.colorize() // thêm màu sắc
		// 	: winston.format.uncolorize(),

		winston.format.splat(),

		// thiết lập định dạng của log
		// winston.format.printf(({ level, message }) => `${level}: ${message}`)
		winston.format.printf((log) => {
			// nếu log là error hiển thị stack trace còn không hiển thị message của log
			if (log.stack) return `[${log.timestamp}] [${log.level}] ${log.stack}`;
			return `[${log.timestamp}] [${log.level}] ${log.message}`;
		})
	),
	transports: configTransports(),
});

module.exports = logger;
