rolesEnum = ['user', 'publisher'];

userRoles = {
	USER: 'user',
	PUBLISHER: 'publisher',
};

module.exports = { rolesEnum, userRoles };
