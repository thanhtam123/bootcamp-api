const { Router } = require('express');

const { userController } = require('../controllers');
const { protect, authorize } = require('../middlewares/auth.middleware');

const router = Router();

router.use(protect, authorize('admin'));

router.route('/').get(userController.getUsers).post(userController.createUser);

router
	.route('/:id')
	.get(userController.getUser)
	.put(userController.updateUser)
	.delete(userController.deleteUser);

module.exports = router;
