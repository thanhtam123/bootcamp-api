const { Router } = require('express');

const { reviewController } = require('../controllers');
const { protect, authorize } = require('../middlewares/auth.middleware');

const router = Router();

router.route('/').get(reviewController.getReviews);

router
	.route('/:id')
	.get(reviewController.getReview)
	.put(protect, authorize('user', 'admin'), reviewController.updateReview)
	.delete(protect, authorize('user', 'admin'), reviewController.deleteReview);

module.exports = router;
