const { Router } = require('express');

const { courseController } = require('../controllers');
const { protect, authorize } = require('../middlewares/auth.middleware');

const router = Router();

router.route('/').get(courseController.getCourses);

router
	.route('/:id')
	.get(courseController.getCourse)
	.put(protect, authorize('publisher', 'admin'), courseController.updateCourse)
	.delete(
		protect,
		authorize('publisher', 'admin'),
		courseController.deleteCourse
	);

module.exports = router;
