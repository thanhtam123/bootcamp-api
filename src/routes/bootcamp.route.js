const { Router } = require('express');

const {
	bootcampController,
	courseController,
	reviewController,
} = require('../controllers');
const { protect, authorize } = require('../middlewares/auth.middleware');

const router = Router();

router
	.route('/')
	.get(bootcampController.getBootcamps)
	.post(
		protect,
		authorize('publisher', 'admin'),
		bootcampController.createBootcamp
	);

// ../bootcamps/radius&zipcode=1&distance=10
router.route('/radius').get(bootcampController.getBootcampsInRadius);

// file upload
router
	.route('/:id/photo')
	.put(
		protect,
		authorize('publisher', 'admin'),
		bootcampController.bootcampPhotoUpload
	);

router
	.route('/:id')
	.get(bootcampController.getBootcamp)
	.put(
		protect,
		authorize('publisher', 'admin'),
		bootcampController.updateBootcamp
	)
	.delete(
		protect,
		authorize('publisher', 'admin'),
		bootcampController.deleteBootcamp
	);

router
	.route('/:bootcampId/course')
	.get(courseController.getCourses)
	.post(protect, authorize('publisher', 'admin'), courseController.addCourse);

router
	.route('/:bootcampId/review')
	.get(reviewController.getReviews)
	.post(protect, authorize('user', 'admin'), reviewController.addReview);

module.exports = router;
