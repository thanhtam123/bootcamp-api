const { Router } = require('express');

const apiRoutes = [
	{
		path: '/bootcamp',
		route: require('./bootcamp.route'),
	},
	{
		path: '/course',
		route: require('./course.route'),
	},
	{
		path: '/auth',
		route: require('./auth.route'),
	},
	{
		path: '/user',
		route: require('./user.route'),
	},
	{
		path: '/review',
		route: require('./review.route'),
	},
];

const configRouter = () => {
	const router = Router();

	apiRoutes.map((route) => {
		router.use(route.path, route.route);
	});

	return router;
};

module.exports = configRouter;
