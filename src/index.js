const dotenv = require('dotenv');

// load env config
const getEnvPath = require('./initials/getEnvPath');
dotenv.config({ path: getEnvPath() });

const app = require('./initials/app');
const { connectDB } = require('./config/db');
const logger = require('./config/logger');

// connect to database
connectDB();

// server listening
const { NODE_ENV, PORT } = process.env;
const server = app.listen(PORT, () => {
	logger.info(
		`Server is listening in ${NODE_ENV.toUpperCase()} environment on port ${PORT}`
	);
});

server.on('close', () => {
	logger.info('Server closed');
});

const exitHandler = () => {
	if (server) {
		server.close(() => {
			// logger.info('Server closed');
			process.exit(1);
		});
	} else {
		process.exit(1);
	}
};

const unexpectedErrorHandler = (error) => {
	logger.error(error);
	exitHandler();
};

process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);

process.on('SIGTERM', () => {
	logger.info('SIGTERM received');
	if (server) {
		server.close();
	}
});

// trigger server closed
process.on('SIGINT', function () {
	server.close(function () {
		process.exit(0);
	});
});
