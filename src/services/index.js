module.exports = {
	bootcampService: require('./bootcamp.service'),
	courseService: require('./course.service'),
	authService: require('./auth.service'),
	userService: require('./user.service'),
	reviewService: require('./review.service'),
};
