const { Bootcamp, User } = require('../models');
const ApiError = require('../utils/ApiError');
const NotFoundApiError = require('../utils/ApiError-custom/NotFound');
const geocoder = require('../utils/geocoder');
// const BadRequestApiError = require('../utils/ApiError-custom/BadRequest');

exports.getBootcamps = async (queryParams = {}) => {
	const {
		count,
		pagination,
		data: bootcamps,
	} = await Bootcamp.performQuery(queryParams);
	return { count, pagination, bootcamps };
};

// exports.getBootcamps = async (queryParams) => {
// 	let query;

// 	// copy request.query
// 	const reqQuery = { ...queryParams };

// 	// field to exclude
// 	const removeFields = ['select', 'sort', 'page', 'limit'];

// 	// loop over removeFields and delete them from reqQuery
// 	removeFields.forEach((params) => delete reqQuery[params]);

// 	// create query string
// 	let queryStr = JSON.stringify(reqQuery);

// 	// create operators ($gt, $gte, etc..)
// 	queryStr = queryStr.replace(
// 		/\b(gt|gte|lt|lte|in)\b/g,
// 		(match) => `$${match}`
// 	);

// 	// finding resource
// 	query = Bootcamp.find(JSON.parse(queryStr));

// 	// select fields
// 	if (queryParams.select) {
// 		const fields = queryParams.select.split(',').join(' ');
// 		query = query.select(fields);
// 	}

// 	// sort
// 	if (queryParams.sort) {
// 		const sortBy = queryParams.sort.split(',').join(' ');
// 		query = query.sort(sortBy);
// 	} else {
// 		query = query.sort('-createdAt');
// 	}

// 	// pagination
// 	const page = parseInt(queryParams.page, 10) || 1;
// 	const limit = parseInt(queryParams.limit, 10) || 100;
// 	const startIndex = (page - 1) * limit;
// 	const endIndex = page * limit;
// 	const total = await Bootcamp.countDocuments();
// 	query = query.skip(startIndex).limit(limit);

// 	// excuting query
// 	const bootcamps = await query;

// 	// pagination result
// 	const pagination = {};
// 	if (endIndex < total) {
// 		pagination.next = {
// 			page: page + 1,
// 			limit,
// 		};
// 	}

// 	if (startIndex > 0) {
// 		pagination.prev = {
// 			page: page - 1,
// 			limit,
// 		};
// 	}

// 	// const bootcamps = await Bootcamp.find();
// 	return { bootcamps, pagination };
// };

exports.getBootcamp = async (id) => {
	const bootcamp = await Bootcamp.findById(id);
	if (!bootcamp) {
		throw new NotFoundApiError(`Not found bootcamp with id ${id}`);
	}
	return bootcamp;
};

exports.createBootcamp = async (createBody) => {
	// Check for published bootcamp
	const publishedBootcamp = await Bootcamp.findOne({
		user: createBody.user.id,
	});

	// If the user is not an admin, they can only add one bootcamp
	if (publishedBootcamp && createBody.user.role !== 'admin') {
		throw new ApiError(
			400,
			`The user with id ${createBody.user.id} has already published a bootcamp`
		);
	}

	// Add user id to createBody
	createBody.user = createBody.user.id;

	const bootcamp = await Bootcamp.create(createBody);
	return bootcamp;
};

exports.updateBootcamp = async (id, updatebody, user) => {
	const bootcamp = await Bootcamp.findById(id);
	if (!bootcamp) {
		throw new NotFoundApiError(`Not found bootcamp with id ${id}`);
	}

	// Make sure user is bootcamp's owner
	if (bootcamp.user.toString() !== user.id && user.role !== 'admin') {
		throw new ApiError(
			401,
			`User ${user.id} is not authorized to update this bootcamp`
		);
	}

	Object.assign(bootcamp, updatebody);
	await bootcamp.save();
	return bootcamp;
};

exports.deleteBootcamp = async (id, user) => {
	// const bootcamp = await Bootcamp.findById(id);
	const bootcamp = await Bootcamp.findById(id);
	if (!bootcamp) {
		throw new NotFoundApiError(`Not found bootcamp with id ${id}`);
	}

	// Make sure user is bootcamp's owner
	if (bootcamp.user.toString() !== user.id && user.role !== 'admin') {
		throw new ApiError(
			401,
			`User ${user.id} is not authorized to delete this bootcamp`
		);
	}

	await bootcamp.remove();

	return true;
};

// get bootcamps within a radius
exports.getBootcampsInRadius = async (radiusBody) => {
	const { zipcode, distance } = radiusBody;

	// get lat/lng from geocoder
	const loc = await geocoder.geocode(zipcode);
	const lat = loc[0].latitude;
	const lng = loc[0].longitude;

	// calc radius using radians
	// Divide dist by radius of earth
	// Earth Radius = 3,963m mi / 6,378 km
	const radius = distance / 3963;

	const bootcamps = await Bootcamp.find({
		location: { $geoWithin: { $centerSphere: [[lng, lat], radius] } },
	});
	return bootcamps;
};

// Upload photo for bootcamp
exports.bootcampPhotoUpload = async (id, file, user) => {
	const bootcamp = await Bootcamp.findById(id);

	if (!bootcamp) {
		throw new NotFoundApiError(`Not found bootcamp with id ${id}`);
	}

	// Make sure user is bootcamp's owner
	if (bootcamp.user.toString() !== user.id && user.role !== 'admin') {
		throw new ApiError(
			401,
			`User ${user.id} is not authorized to upload photo for this bootcamp`
		);
	}

	// Save photo
	try {
		await file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`);
		bootcamp.photo = file.name;
		await bootcamp.save();
		return bootcamp;
	} catch (err) {
		throw new ApiError(500, `Problem with file upload.`);
	}
};
