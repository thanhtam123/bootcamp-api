const { User } = require('../models');
const ApiError = require('../utils/ApiError');
const NotFoundApiError = require('../utils/ApiError-custom/NotFound');
const sendEmail = require('../utils/sendEmail');

exports.register = async (registerBody) => {
	const { name, email, password, role } = registerBody;

	// create user
	const user = await User.create({
		name,
		email,
		password,
		role,
	});

	// create jwt token
	// const token = user.getSignedJwtToken();

	return { user };
};

exports.login = async (loginBody) => {
	const { email, password } = loginBody;

	const user = await User.findOne({ email });
	if (!user || !(await user.matchPassword(password))) {
		throw new ApiError(401, 'Invalid credentials');
	}

	return { user };
};

exports.logout = async () => {};

exports.forgotPassword = async (email, req) => {
	const user = await User.findOne({ email: email });

	if (!user) {
		throw new ApiError(404, `Not found user with email ${email}`);
	}

	// Get reset password token
	const resetToken = user.getResetPasswordToken();

	await user.save({ validateBeforeSave: false });

	// create reset url
	const resetUrl = `${req.protocol}://${req.get(
		'host'
	)}/auth/resetpassword/${resetToken}`;

	const message = `You are receiving this email because you (or someone else) has requested the reset of a password. Please make a PUT request to: \n\n ${resetUrl}`;

	try {
		await sendEmail({
			email: user.email,
			subject: 'Password reset token',
			message,
		});

		return true;
	} catch (err) {
		user.resetPasswordToken = undefined;
		user.resetPasswordExpire = undefined;
		await user.save({ validateBeforeSave: false });
		throw new Error(err);
	}
};

exports.resetPassword = async (resetPasswordToken, newPassword) => {
	const user = await User.findOne({
		resetPasswordToken,
		resetPasswordExpire: { $gt: Date.now() },
	});

	if (!user) {
		throw new ApiError('Invalid token.', 400);
	}

	// set new password
	user.password = newPassword;
	user.resetPasswordToken = undefined;
	user.resetPasswordExpire = undefined;
	await user.save();

	return user;
};

exports.updateDetails = async (id, updateBody) => {
	const { email, name } = updateBody;

	const user = await User.findByIdAndUpdate(id, updateBody, {
		new: true,
		runValidators: true,
	});

	return user;
};

exports.updatePassword = async (id, currentPassword, newPassword) => {
	const user = await User.findById(id);

	if (!(await user.matchPassword(currentPassword))) {
		throw new ApiError(401, 'Password is incorrect');
	}

	user.password = newPassword;
	await user.save();

	return user;
};
