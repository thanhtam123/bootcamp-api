const { User } = require('../models');
// const ApiError = require('../utils/ApiError');
const NotFoundApiError = require('../utils/ApiError-custom/NotFound');

exports.getUsers = async (queryParams = {}) => {
	const {
		count,
		pagination,
		data: users,
	} = await User.performQuery(queryParams);
	return { count, pagination, users };
};

exports.getUser = async (id) => {
	const user = await User.findById(id);
	if (!user) {
		throw new NotFoundApiError(`Not found user with id of ${id}`);
	}
	return user;
};

exports.createUser = async (createBody) => {
	const user = await User.create(createBody);
	return user;
};

exports.updateUSer = async (id, updateBody) => {
	const user = await User.findByIdAndUpdate(id, updateBody, {
		new: true,
		runValidators: true,
	});

	return user;
};

exports.deleteUser = async (id) => {
	await User.findByIdAndDelete(id);

	return true;
};
