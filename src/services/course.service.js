const { Course, Bootcamp } = require('../models');
const ApiError = require('../utils/ApiError');
const NotFoundApiError = require('../utils/ApiError-custom/NotFound');

exports.getCoursesByBootcampId = async (bootcampId) => {
	const courses = await Course.find({ bootcamp: bootcampId });
	return courses;
};

exports.getCourses = async (queryParams = {}) => {
	// const courses = await Course.find();
	const {
		count,
		pagination,
		data: courses,
	} = await Course.performQuery(queryParams);
	return { count, pagination, courses };
};

exports.getCourse = async (id) => {
	const course = await Course.findById(id);
	if (!course) {
		throw new NotFoundApiError(`Not found course with id of ${id}`);
	}
	return course;
};

exports.addCourse = async (createBody, user) => {
	const { bootcamp: bootcampId } = createBody;
	const bootcamp = await Bootcamp.findById(bootcampId);
	if (!bootcamp) {
		throw new NotFoundApiError(`Not found bootcamp with id ${bootcampId}`);
	}

	// Make sure user is bootcamp's owner
	if (bootcamp.user.toString() !== user.id && user.role !== 'admin') {
		throw new ApiError(
			401,
			`User ${user.id} is not authorized to add course to bootcamp ${bootcamp.id}`
		);
	}

	const course = await Course.create(createBody);
	return course;
};

exports.updateCourse = async (id, updateBody, user) => {
	let course = await Course.findById(id);
	if (!course) {
		throw new NotFoundApiError(`Not found course with id of ${id}`);
	}

	// Make sure user is course's owner
	if (course.user.toString() !== user.id && user.role !== 'admin') {
		throw new ApiError(
			401,
			`User ${user.id} is not authorized to update this course.`
		);
	}

	course = await Course.findByIdAndUpdate(id, updateBody, {
		new: true,
		runValidators: true,
	});

	await course.save();

	return course;
};

exports.deleteCourse = async (id, user) => {
	const course = await Course.findById(id);
	if (!course) {
		throw new NotFoundApiError(`Not found course with id of ${id}`);
	}
	// Make sure user is course's owner
	if (course.user.toString() !== user.id && user.role !== 'admin') {
		throw new ApiError(
			401,
			`User ${user.id} is not authorized to delete this course.`
		);
	}
	await course.remove();
	return true;
};
