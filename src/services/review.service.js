const { Review, Bootcamp } = require('../models');
const ApiError = require('../utils/ApiError');
const NotFoundApiError = require('../utils/ApiError-custom/NotFound');

exports.getReviewsByBootcampId = async (bootcampId) => {
	const reviews = await Review.find({ bootcamp: bootcampId });
	return reviews;
};

exports.getReviews = async (
	queryParams = { path: 'bootcamp', select: 'name deacription' }
) => {
	const {
		count,
		pagination,
		data: reviews,
	} = await Review.performQuery(queryParams, {
		path: 'bootcamp',
		select: 'name deacription',
	});
	return { count, pagination, reviews };
};

exports.getReview = async (id) => {
	const review = await Review.findById(id).populate({
		path: 'bootcamp',
		select: 'name deacription',
	});
	if (!review) {
		throw new NotFoundApiError(`Not found review with id of ${id}`);
	}
	return review;
};

exports.addReview = async (createBody) => {
	const { bootcamp: bootcampId } = createBody;
	const bootcamp = await Bootcamp.findById(bootcampId);
	if (!bootcamp) {
		throw new NotFoundApiError(`Not found bootcamp with id ${bootcampId}`);
	}

	const review = await Review.create(createBody);
	return review;
};

exports.updateReview = async (id, updateBody, user) => {
	let review = await Review.findById(id);
	if (!review) {
		throw new NotFoundApiError(`Not found review with id of ${id}`);
	}

	// Make sure user is review's owner
	if (review.user.toString() !== user.id && user.role !== 'admin') {
		throw new ApiError(
			401,
			`User ${user.id} is not authorized to update this review.`
		);
	}

	review = await Review.findByIdAndUpdate(id, updateBody, {
		new: true,
		runValidators: true,
	});

	await review.save();

	return review;
};

exports.deleteReview = async (id, user) => {
	const review = await Review.findById(id);
	if (!review) {
		throw new NotFoundApiError(`Not found review with id of ${id}`);
	}
	// Make sure user is review's owner
	if (review.user.toString() !== user.id && user.role !== 'admin') {
		throw new ApiError(
			401,
			`User ${user.id} is not authorized to delete this review.`
		);
	}
	await review.remove();
	return true;
};
