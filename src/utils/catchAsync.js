const catchAsync = (fn, options = {}) => {
	// use custome response
	const { useCustomerResponse } = options;
	if (useCustomerResponse) {
		return (req, res, next) =>
			Promise.resolve(fn(req, res, next)).catch((err) => next(err));
	}

	let { statusCode } = options;
	// status code for when success
	statusCode = statusCode ? statusCode : 200;

	return (req, res, next) =>
		Promise.resolve(fn(req, res, next))
			.then(({ success, ...result }) => {
				// response status
				let resStatus = success ? success : true;

				res.status(statusCode).send({
					success: resStatus,
					...result,
				});
			})
			.catch((err) => next(err));
};

module.exports = catchAsync;
