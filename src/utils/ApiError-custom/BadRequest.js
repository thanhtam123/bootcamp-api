const httpStatus = require('http-status');
const ApiError = require('../ApiError');

class BadRequestApiError extends ApiError {
	constructor(message) {
		super(httpStatus.BAD_REQUEST, message);
	}
}

module.exports = BadRequestApiError;
