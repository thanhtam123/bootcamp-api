const catchAsync = require('../utils/catchAsync');
const httpStatus = require('http-status');
const { bootcampService } = require('../services');
const NotFoundApiError = require('../utils/ApiError-custom/NotFound');
const ApiError = require('../utils/ApiError');
const path = require('path');

exports.getBootcamps = catchAsync(async (req, res, next) => {
	const { count, bootcamps, pagination } = await bootcampService.getBootcamps(
		req.query
	);
	return { count, pagination, bootcamps };
});

// @desc    Get single bootcamp
// @route   GET ../bootcamp/:id
// @access  Public
exports.getBootcamp = catchAsync(async (req, res, next) => {
	const { id } = req.params;
	const bootcamps = await bootcampService.getBootcamp(id);
	return { bootcamps };
});

// @desc    Create new bootcamp
// @route   POST ../bootcamp
// @access  Private
exports.createBootcamp = catchAsync(
	async (req, res, next) => {
		// Add user to req.body
		req.body.user = req.user;

		const bootcamp = await bootcampService.createBootcamp(req.body);
		return { bootcamp };
	},
	{
		statusCode: httpStatus.CREATED,
	}
);

// @desc    Update bootcamp
// @route   PUT ../bootcamp/:id
// @access  Public
exports.updateBootcamp = catchAsync(async (req, res, next) => {
	const { id } = req.params;
	const bootcamp = await bootcampService.updateBootcamp(id, req.body, req.user);
	return { bootcamp };
});

// @desc    Delete bootcamp
// @route   DELETE ../bootcamp/:id
// @access  Private
exports.deleteBootcamp = catchAsync(async (req, res, next) => {
	const { id } = req.params;
	await bootcampService.deleteBootcamp(id, req.user);
	return { bootcamp: {} };
});

// @desc    Get bootcamps within a radius
// @route   GET ../bootcamp/radius&zipcode=1&distance=10
// @access  Private
exports.getBootcampsInRadius = catchAsync(async (req, res, next) => {
	const bootcamps = await bootcampService.getBootcampsInRadius(req.query);
	return { count: bootcamps.length, bootcamps };
});

// @desc    Upload photo for bootcamp
// @route   PUT ../bootcamp/:id/photo
// @access  Private
exports.bootcampPhotoUpload = catchAsync(async (req, res, next) => {
	if (!req.files || Object.keys(req.files).length === 0) {
		throw new NotFoundApiError('No files were uploaded.');
	}

	const { file } = req.files;

	// make sure the image is a photo
	if (!file.mimetype.startsWith('image')) {
		throw new ApiError(400, 'Please upload an image file.');
	}

	// check file size
	if (file.size > process.env.MAX_FILE_UPLOAD) {
		throw new ApiError(
			400,
			`Please upload an image less than ${process.env.MAX_FILE_UPLOAD}.`
		);
	}

	const { id } = req.params;
	file.name = `photo_bootcamp_${id}${path.parse(file.name).ext}`;

	const bootcamp = await bootcampService.bootcampPhotoUpload(
		id,
		file,
		req.user
	);
	return { bootcamp };
});
