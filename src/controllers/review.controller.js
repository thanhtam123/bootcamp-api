const catchAsync = require('../utils/catchAsync');
const httpStatus = require('http-status');
const { reviewService } = require('../services');
// const NotFoundApiError = require('../utils/ApiError-custom/NotFound');
// const ApiError = require('../utils/ApiError');
// const path = require('path');

// @desc      Get reviews
// @route     GET /review
// @route     GET /bootcamps/:bootcampId/review
// @access    Public
exports.getReviews = catchAsync(async (req, res, next) => {
	const { bootcampId } = req.params;
	if (bootcampId) {
		const reviews = await reviewService.getReviewsByBootcampId(bootcampId);
		return { count: reviews.length, reviews };
	}

	const { count, pagination, reviews } = await reviewService.getReviews(
		req.query
	);
	return { count, pagination, reviews };
});

// @desc      Get single review
// @route     GET /review/:id
// @access    Public
exports.getReview = catchAsync(async (req, res, next) => {
	const { id } = req.params;
	const review = await reviewService.getReview(id);
	return { review };
});

// @desc      Add review
// @route     POST /bootcamps/:bootcampId/review
// @access    Private
exports.addReview = catchAsync(async (req, res, next) => {
	const review = await reviewService.addReview(
		{
			...req.body,
			bootcamp: req.params.bootcampId,
			user: req.user.id,
		},
		req.user
	);
	return { review };
});

// @desc      Update review
// @route     PUT /review/:id
// @access    Private
exports.updateReview = catchAsync(async (req, res, next) => {
	const { id } = req.params;
	const review = await reviewService.updateReview(id, req.body, req.user);
	return { review };
});

// @desc      Delete review
// @route     DELETE /review/:id
// @access    Private
exports.deleteReview = catchAsync(async (req, res, next) => {
	const { id } = req.params;
	await reviewService.deleteReview(id, req.user);
	return { review: {} };
});
