const catchAsync = require('../utils/catchAsync');
const httpStatus = require('http-status');
const { courseService } = require('../services');

// @desc      Get courses
// @route     GET /course
// @route     GET /bootcamps/:bootcampId/course
// @access    Public
exports.getCourses = catchAsync(async (req, res, next) => {
	const { bootcampId } = req.params;
	if (bootcampId) {
		const courses = await courseService.getCoursesByBootcampId(bootcampId);
		return { count: courses.length, courses };
	}

	const { count, pagination, courses } = await courseService.getCourses(
		req.query
	);
	return { count, pagination, courses };
});

// @desc      Get single course
// @route     GET /course/:id
// @access    Public
exports.getCourse = catchAsync(async (req, res, next) => {
	const { id } = req.params;
	const course = await courseService.getCourse(id);
	return { course };
});

// @desc      Add course
// @route     POST /bootcamps/:bootcampId/course
// @access    Private
exports.addCourse = catchAsync(async (req, res, next) => {
	const course = await courseService.addCourse(
		{
			...req.body,
			bootcamp: req.params.bootcampId,
			user: req.user.id,
		},
		req.user
	);
	return { course };
});

// @desc      Update course
// @route     PUT /course/:id
// @access    Private
exports.updateCourse = catchAsync(async (req, res, next) => {
	const { id } = req.params;
	const course = await courseService.updateCourse(id, req.body, req.user);
	return { course };
});

// @desc      Delete course
// @route     DELETE /course/:id
// @access    Private
exports.deleteCourse = catchAsync(async (req, res, next) => {
	const { id } = req.params;
	await courseService.deleteCourse(id, req.user);
	return { course: {} };
});
