const catchAsync = require('../utils/catchAsync');
const httpStatus = require('http-status');
const { userService } = require('../services');

// @desc      Get all users
// @route     GET //user
// @access    Private/Admin
exports.getUsers = catchAsync(async (req, res, next) => {
	const { count, pagination, users } = await userService.getUsers(req.query);
	return { count, pagination, users };
});

// @desc      Get single user
// @route     GET //user/:id
// @access    Private/Admin
exports.getUser = catchAsync(async (req, res, next) => {
	const user = await userService.getUser(req.params.id);
	return { user };
});

// @desc      Create user
// @route     POST //user
// @access    Private/Admin
exports.createUser = catchAsync(async (req, res, next) => {
	const user = await userService.createUser(req.body);

	return {
		user,
	};
});

// @desc      Update user
// @route     PUT //user/:id
// @access    Private/Admin
exports.updateUser = catchAsync(async (req, res, next) => {
	const user = await userService.updateUSer(req.params.id, req.body);

	return { user };
});

// @desc      Delete user
// @route     DELETE //user/:id
// @access    Private/Admin
exports.deleteUser = catchAsync(async (req, res, next) => {
	await userService.deleteUser(req.params.id);

	return { user: {} };
});
