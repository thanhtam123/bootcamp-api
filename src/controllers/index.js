module.exports = {
	bootcampController: require('./bootcamp.controller'),
	courseController: require('./course.controller'),
	authController: require('./auth.controller'),
	userController: require('./user.controller'),
	reviewController: require('./review.controller'),
};
