const catchAsync = require('../utils/catchAsync');
const httpStatus = require('http-status');
const { authService } = require('../services');
const crypto = require('crypto');

// @desc      Register user
// @route     POST /auth/register
// @access    Public
exports.register = catchAsync(
	async (req, res, next) => {
		const { user } = await authService.register(req.body);
		sendTokenResponse(user, 200, res);
	},
	{
		// statusCode: httpStatus.CREATED,
		useCustomerResponse: true,
	}
);

// @desc      Login user
// @route     POST /auth/login
// @access    Public
exports.login = catchAsync(
	async (req, res, next) => {
		const { user } = await authService.login(req.body);

		sendTokenResponse(user, 200, res);
	},
	{
		useCustomerResponse: true,
	}
);

// @desc      Log user out / clear cookie
// @route     GET /auth/logout
// @access    Public
exports.logout = catchAsync(async (req, res, next) => {
	res.cookie('token', 'none', {
		expires: new Date(Date.now() + 10 * 1000),
		httpOnly: true,
	});

	return { user: {} };
});

// @desc      Get current logged in user
// @route     GET /auth/me
// @access    Private
exports.getMe = catchAsync(async (req, res, next) => {
	// user is already available in req due to the protect middleware
	const user = req.user;
	return { user };
});

// @desc      Update user details
// @route     PUT /auth/updatedetails
// @access    Private
exports.updateDetails = catchAsync(async (req, res, next) => {
	const { id: userId } = req.user;
	const user = await authService.updateDetails(userId, req.body);
	return { user };
});

// @desc      Update password
// @route     PUT /auth/updatepassword
// @access    Private
exports.updatePassword = catchAsync(
	async (req, res, next) => {
		const { currentPassword, newPassword } = req.body;
		const { id: userId } = req.user;
		const user = await authService.updatePassword(
			userId,
			currentPassword,
			newPassword
		);

		sendTokenResponse(user, 200, res);
	},
	{
		useCustomerResponse: true,
	}
);

// @desc      Forgot password
// @route     POST /auth/forgotpassword
// @access    Public
exports.forgotPassword = catchAsync(async (req, res, next) => {
	const { email } = req.body;
	await authService.forgotPassword(email, req);
	return { message: 'Email send ...' };
});

// @desc      Reset password
// @route     PUT /auth/resetpassword/:resettoken
// @access    Public
exports.resetPassword = catchAsync(
	async (req, res, next) => {
		// Get hashed token
		const resetPasswordToken = crypto
			.createHash('sha256')
			.update(req.params.resettoken)
			.digest('hex');

		const user = await authService.resetPassword(
			resetPasswordToken,
			req.body.password
		);

		sendTokenResponse(user, 200, res);
	},
	{
		useCustomerResponse: true,
	}
);

/**
 * @desc    Confirm Email
 * @route   GET /auth/confirmemail
 * @access  Public
 */
exports.confirmEmail = catchAsync(async (req, res, next) => {
	// grab token from email
});

// Get token from model, create cookie and send response
const sendTokenResponse = (user, statusCode, res) => {
	// create jwt token
	const token = user.getSignedJwtToken();

	const options = {
		expires: new Date(
			Date.now() + process.env.JWT_COOKIE_EXPIRE * 24 * 60 * 60
		),
		httpOnly: true,
	};

	if (process.env.NODE_ENV === 'production') {
		options.secure = truel;
	}

	res
		.status(statusCode)
		.cookie('token', token, options)
		.send({ success: true, user, token });
};
