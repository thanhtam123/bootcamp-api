const jwt = require('jsonwebtoken');
const catchAsync = require('../utils/catchAsync');
const ApiError = require('../utils/ApiError');
const { User } = require('../models');

// Protect routes
exports.protect = catchAsync(
	async (req, res, next) => {
		let token;

		const { authorization } = req.headers;
		if (authorization && authorization.startsWith('Bearer')) {
			// set token from Bearer token
			token = authorization.split(' ')[1];
		} else if (req.cookies.token) {
			// Set token from cookie in header
			token = req.cookies.token;
		}

		// Make sure token exists
		if (!token) {
			throw new ApiError(401, 'Not authorize to access this route.');
		}

		try {
			// Verify token
			const decoded = jwt.verify(token, process.env.JWT_SECRET);

			req.user = await User.findById(decoded.id);

			if (!req.user) {
				throw new ApiError(401, 'Not authorize to access this route.');
			}

			next();
		} catch (err) {
			throw new ApiError(401, 'Not authorize to access this route.');
		}
	},
	{
		useCustomerResponse: true,
	}
);

// Grant access to specific roles
exports.authorize = (...roles) => {
	return catchAsync(
		async (req, res, next) => {
			if (!roles.includes(req.user.role)) {
				throw new ApiError(
					401,
					`User role ${req.user.role} is not authorized to access this route.`
				);
			}
			next();
		},
		{
			useCustomerResponse: true,
		}
	);
};
