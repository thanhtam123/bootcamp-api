const httpStatus = require('http-status');
const logger = require('../config/logger');
const ApiError = require('../utils/ApiError');

const errorConverter = async (err, req, res, next) => {
	let error = err;
	if (!(error instanceof ApiError)) {
		const { statusCode, message } = transformError(err);
		error = new ApiError(statusCode, message, false, err.stack);
	}
	next(error);
};

// eslint-disable-next-line no-unused-vars
const errorHandler = async (err, req, res, next) => {
	let { statusCode, message } = err;
	// if (process.env.NODE_ENV === 'production' && !err.isOperational) {
	// 	statusCode = httpStatus.INTERNAL_SERVER_ERROR;
	// 	message = httpStatus[httpStatus.INTERNAL_SERVER_ERROR];
	// }

	res.locals.errorMessage = err.message;

	const response = {
		success: false,
		code: statusCode,
		message,
		...(process.env.NODE_ENV === 'development' && { stack: err.stack }),
	};

	if (process.env.env === 'development') {
		logger.error(err);
	}

	res.status(statusCode).send(response);
};

const transformError = (err) => {
	let { statusCode, message } = err;
	if (!statusCode) {
		const { name: errName } = err;
		switch (errName) {
			case 'CastError':
				statusCode = httpStatus.NOT_FOUND;
				message = 'Resource not found';
				break;

			case 'ValidationError':
				statusCode = httpStatus.BAD_REQUEST;
				break;

			case 'MongoError':
				if (err.code === 11000) {
					// Duplicated Field Error
					statusCode = httpStatus.BAD_REQUEST;
					message = transformDupicatedFieldErrorMessage(err);
				} else {
					statusCode = httpStatus.INTERNAL_SERVER_ERROR;
				}
				break;

			default:
				statusCode = httpStatus.INTERNAL_SERVER_ERROR;
				break;
		}
	}

	message = message || httpStatus[statusCode];
	return { statusCode, message };
};

const transformDupicatedFieldErrorMessage = (err) => {
	if (err.code !== 11000) return '';

	let message = [];
	const { keyValue } = err;
	for (let key in keyValue) {
		message = [...message, `${key} \"${err.keyValue[key]}\" is already in use`];
	}
	return message.toString();
};

module.exports = {
	errorConverter,
	errorHandler,
};
